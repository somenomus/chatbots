Chatbots Training with Reinforcement Learning

In this chatbot training, I used a dataset of movies. The dataset called “Cornell Movie-Dialogs Corpus” which contains structured dialogs extracted from various movies.

A bit about different program:

Two  modules working with the dataset used to train the model are cornell.py and data.py. Both are for data processing which was used for the transformation of the dataset into a form suitable for training but working on different layers.
The cornell.py file includes low-level functions to parse data in the Cornell Movie-Dialogs Corpus format and represents it in a form of suitable. The main goal of the module is to load a list of dialogs from movies. As the dataset contains metadata about the movies, we can filter dialogs to be loaded by various criteria, but only a genre filter is implemented.

To calculate the BLEU score, the nltk library was used.

The functions related to the training process and the model itself are defined in the libbots/model.py file.

Now,to train the first approximation of the model, the cross-entropy method is used and implemented in train_crossentropy.py. During the training, it will randomly switch between the teacher-forcing mode and argmax chain decoding with the probability of 50%.

Training:

Now, the hole datasets contains 617 movies. As I trained on my Laptop which have GTX 970M GPU with 6GB of RAM, it would take weeks to complete. So, I trained my model with only ‘crime’ genre movies which contains 147 movies. 

Then, 2nd part. RL training is implemented as a separate training step in the tool train_scst.py. It requires the model file saved by train_crossentropy.py to be passed in a command line.

Testing:

During the training, the training tools (train_scst.py) periodically save the model, which is done in two different situations: when the BLEU score on the test dataset updates the maximum and every 10 epoches. Both kinds of models have the same format (produced by the torch.save() method) and contain the model's weights.

use_model.py, which will load the model and need to pass a sentence. And then, it will generate an answer using the model.

For example, passing - 

$ ./use_model.py -m saves/model.dat -s 'how are you?' --self 5  

Will give output as- 

very well. thank you.
okay ... it's fine.
hey ...
shut up.
fair enough..

Telegram bot:

I tried to test using the Telegram bot, but due to secure connection, SSL error, I was unable to debug. core.telegram.org was not reachable as well as the API. I tried to use a VPN, but maybe due to DNS leak, same error occurred.




